<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Controllers\DatabaseController as Conn;
use App\Models\SaleModel as Sale;
use PDO;

class SaleRepository
{
    private PDO $conn;

    public function __construct()
    {
        $db = new Conn();
        $this->conn = $db->getConnection();
    }

    public function all(): array
    {
        $sql = $this->conn->prepare("SELECT id, address, datesale FROM sales");
        $sql->execute([]);
        return $sql->fetchAll();
    }

    public function find(Sale $sale): array
    {
        $sql = $this->conn->prepare("SELECT id, address, datesale
                                        FROM sales WHERE id = :id");
        $sql->execute([
            'id' => $sale->getId()
        ]);
        return $sql->fetchAll();
    }

    public function insert(Sale $sale): array
    {
        $sql = $this->conn->prepare("INSERT INTO sales (id, address, datesale)
                                        VALUES (DEFAULT, UPPER(:address), DEFAULT)
                                        RETURNING *");
        $sql->execute([
            'address' => $sale->getAddress(),
        ]);
        return $sql->fetchAll();
    }

    public function change(Sale $sale): array
    {
        $sql = $this->conn->prepare("UPDATE sales
                                        SET address = UPPER(:address)
                                        WHERE id = :id RETURNING *");
        $sql->execute([
            'id' => $sale->getId(),
            'address' => $sale->getAddress(),
        ]);
        return $sql->fetchAll();
    }

    public function remove(Sale $sale): array
    {
        $sql = $this->conn->prepare("DELETE FROM sales WHERE id = :id RETURNING *");
        $sql->execute([
            'id' => $sale->getId()
        ]);
        return $sql->fetchAll();
    }

    public function report(): array
    {
        $sql = $this->conn->prepare("SELECT customer_id, customer_name, customer_lastname, customer_age,
                                            sale_id, sale_address, sale_date, product_id,
                                            product_ref, product_name, product_value, product_quantity,
                                            product_discount FROM v_sales");
        $sql->execute([]);
        return $sql->fetchAll();
    }
}
