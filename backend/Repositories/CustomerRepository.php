<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Controllers\DatabaseController as Conn;
use App\Models\CustomerModel as Customer;
use PDO;

class CustomerRepository
{
    private PDO $conn;

    public function __construct()
    {
        $db = new Conn();
        $this->conn = $db->getConnection();
    }

    public function all(): array
    {
        $sql = $this->conn->prepare("SELECT id, name, lastname, age FROM customers");
        $sql->execute([]);
        return $sql->fetchAll();
    }

    public function find(Customer $customer): array
    {
        $sql = $this->conn->prepare("SELECT id, name, lastname, age
                                        FROM customers WHERE id = :id");
        $sql->execute([
            'id' => $customer->getId()
        ]);
        return $sql->fetchAll();
    }

    public function insert(Customer $customer): array
    {
        $sql = $this->conn->prepare("INSERT INTO customers (id, name, lastname, age)
                                        VALUES (DEFAULT, INITCAP(:name), INITCAP(:lastname), :age)
                                        RETURNING *");
        $sql->execute([
            'name' => $customer->getName(),
            'lastname' => $customer->getLastname(),
            'age' => $customer->getAge()
        ]);
        return $sql->fetchAll();
    }

    public function change(Customer $customer): array
    {
        $sql = $this->conn->prepare("UPDATE customers
                                        SET name = INITCAP(:name), lastname = INITCAP(:lastname), age = :age
                                        WHERE id = :id RETURNING *");
        $sql->execute([
            'id' => $customer->getId(),
            'name' => $customer->getName(),
            'lastname' => $customer->getLastname(),
            'age' => $customer->getAge()
        ]);
        return $sql->fetchAll();
    }

    public function remove(Customer $customer): array
    {
        $sql = $this->conn->prepare("DELETE FROM customers WHERE id = :id RETURNING *");
        $sql->execute([
            'id' => $customer->getId()
        ]);
        return $sql->fetchAll();
    }
}
