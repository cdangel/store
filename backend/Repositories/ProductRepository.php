<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Controllers\DatabaseController as Conn;
use App\Models\ProductModel as Product;
use PDO;

class ProductRepository
{
    private PDO $conn;

    public function __construct()
    {
        $db = new Conn();
        $this->conn = $db->getConnection();
    }

    public function all(): array
    {
        $sql = $this->conn->prepare("SELECT id, ref, name, value FROM products");
        $sql->execute([]);
        return $sql->fetchAll();
    }

    public function find(Product $product): array
    {
        $sql = $this->conn->prepare("SELECT id, ref, name, value
                                        FROM products WHERE id = :id");
        $sql->execute([
            'id' => $product->getId()
        ]);
        return $sql->fetchAll();
    }

    public function insert(Product $product): array
    {
        $sql = $this->conn->prepare("INSERT INTO products (id, ref, name, value)
                                        VALUES (DEFAULT, UPPER(:ref), INITCAP(:name), :value)
                                        RETURNING *");
        $sql->execute([
            'ref' => $product->getRef(),
            'name' => $product->getName(),
            'value' => $product->getValue()
        ]);
        return $sql->fetchAll();
    }

    public function change(Product $product): array
    {
        $sql = $this->conn->prepare("UPDATE products
                                        SET ref = INITCAP(:ref), name = INITCAP(:name), value = :value
                                        WHERE id = :id RETURNING *");
        $sql->execute([
            'id' => $product->getId(),
            'ref' => $product->getRef(),
            'name' => $product->getName(),
            'value' => $product->getValue()
        ]);
        return $sql->fetchAll();
    }

    public function remove(Product $product): array
    {
        $sql = $this->conn->prepare("DELETE FROM products WHERE id = :id RETURNING *");
        $sql->execute([
            'id' => $product->getId()
        ]);
        return $sql->fetchAll();
    }
}
