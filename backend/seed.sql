INSERT INTO customers (id, name, lastname, age) VALUES (DEFAULT, INITCAP('daniel'), INITCAP('angel'), 23);
INSERT INTO customers (id, name, lastname, age) VALUES (DEFAULT, INITCAP('johanna'), INITCAP('torres'), 26);

INSERT INTO products (id, ref, name, value) VALUES (DEFAULT, UPPER('R729'), INITCAP('camisa azul'), 50000);
INSERT INTO products (id, ref, name, value) VALUES (DEFAULT, UPPER('R730'), INITCAP('camisa verde'), 55000);

INSERT INTO sales (id, address, datesale) VALUES (DEFAULT, UPPER('Pereira Cll 21 Mz 5 No 20-14'), DEFAULT);
INSERT INTO sales (id, address, datesale) VALUES (DEFAULT, UPPER('Pereira Cll 36 Mz 6 No 18-11'), DEFAULT);
INSERT INTO sales (id, address, datesale) VALUES (DEFAULT, UPPER('Pereira Cll 2 Mz 1 No 1-45'), DEFAULT);

INSERT INTO central (id, customer, product, sale, quantity, discount) VALUES (DEFAULT, 1, 1, 1, 3, 0);
INSERT INTO central (id, customer, product, sale, quantity, discount) VALUES (DEFAULT, 1, 2, 1, 1, 0);

INSERT INTO central (id, customer, product, sale, quantity, discount) VALUES (DEFAULT, 2, 1, 2, 5, 10);
INSERT INTO central (id, customer, product, sale, quantity, discount) VALUES (DEFAULT, 2, 2, 2, 2, 0);

INSERT INTO central (id, customer, product, sale, quantity, discount) VALUES (DEFAULT, 1, 2, 3, 7, 10);