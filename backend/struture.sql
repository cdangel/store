create table customers (
    id SERIAL PRIMARY KEY NOT NULL,
    name VARCHAR NOT NULL,
    lastname VARCHAR NOT NULL,
    age INT NOT NULL
);

create table products (
    id SERIAL PRIMARY KEY NOT NULL,
    ref VARCHAR NOT NULL,
    name VARCHAR NOT NULL,
    value INT NOT NULL
);

create table sales (
    id SERIAL PRIMARY KEY NOT NULL,
    address VARCHAR NOT NULL,
    datesale TIMESTAMP DEFAULT NOW()
);

create table central (
    id SERIAL PRIMARY KEY NOT NULL,
    customer INT NOT NULL,
    product INT NOT NULL,
    sale INT NOT NULL,
    quantity INT NOT NULL,
    discount INT NOT NULL,
    FOREIGN KEY (customer) REFERENCES customers(id),
    FOREIGN KEY (product) REFERENCES products(id),
    FOREIGN KEY (sale) REFERENCES sales(id)
);

CREATE VIEW v_sales AS
    SELECT
        a.id AS customer_id, a.name AS customer_name, a.lastname AS customer_lastname, a.age AS customer_age,
        b.id AS sale_id, b.address AS sale_address, b.datesale AS sale_date,
        c.id AS product_id, c.ref AS product_ref, c.name AS product_name, c.value AS product_value,
        d.quantity AS product_quantity, d.discount AS product_discount
        FROM central AS d
            INNER JOIN products AS c ON c.id = d.product
            INNER JOIN sales AS b ON b.id = d.sale
            INNER JOIN customers AS a ON a.id = d.customer;