<?php

declare(strict_types=1);

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE');
header('Accept: application/json');
header('Content-Type: application/json');

require_once '../vendor/autoload.php';

use App\Controllers\SaleController;

function router(): void
{
    $response = [];
    $controller = new SaleController();

    switch ($_SERVER['REQUEST_METHOD']) {
        case 'GET':
            if (isset($_GET['id']))
                $response = $controller->show($_GET);
            else if(isset($_GET['report']))
                $response = $controller->reportSale();
            else
                $response = $controller->index();
            break;

        case 'POST':
            $_POST = json_decode(file_get_contents('PHP://input'), true);
            if (count($_POST) > 0)
                $response = $controller->store($_POST);
            break;

        case 'PUT':
            $_PUT = json_decode(file_get_contents('PHP://input'), true);
            if (count($_PUT) > 0)
                $response = $controller->update($_PUT);
            break;

        case 'DELETE':
            $_DELETE = json_decode(file_get_contents('PHP://input'), true);
            if (isset($_DELETE['id']))
                $response = $controller->delete($_DELETE);
            break;
    }

    echo json_encode($response);
}

router();
