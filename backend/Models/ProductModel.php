<?php

declare(strict_types=1);

namespace App\Models;

class ProductModel
{
    private int $id;
    private string $ref;
    private string $name;
    private int $value;

    function getId(): int
    {
        return $this->id;
    }

    function getRef(): string
    {
        return $this->ref;
    }

    function getName(): string
    {
        return $this->name;
    }

    function getValue(): int
    {
        return $this->value;
    }

    function setId(int $id): void
    {
        $this->id = $id;
    }

    function setRef(string $ref): void
    {
        $this->ref = $ref;
    }

    function setName(string $name): void
    {
        $this->name = $name;
    }

    function setValue(int $value): void
    {
        $this->value = $value;
    }
}
