<?php

declare(strict_types=1);

namespace App\Models;

class CustomerModel
{
    private int $id;
    private string $name;
    private string $lastname;
    private int $age;

    function getId(): int
    {
        return $this->id;
    }

    function getName(): string
    {
        return $this->name;
    }

    function getLastname(): string
    {
        return $this->lastname;
    }

    function getAge(): int
    {
        return $this->age;
    }

    function setId(int $id): void
    {
        $this->id = $id;
    }

    function setName($name): void
    {
        $this->name = $name;
    }

    function setLastname($lastname): void
    {
        $this->lastname = $lastname;
    }

    function setAge(int $age): void
    {
        $this->age = $age;
    }
}
