<?php

declare(strict_types=1);

namespace App\Models;

class SaleModel
{
    private int $id;
    private string $address;
    private string $date;

    function getId(): int
    {
        return $this->id;
    }

    function getAddress(): string
    {
        return $this->address;
    }

    function getDate(): string
    {
        return $this->date;
    }

    function setId(int $id): void
    {
        $this->id = $id;
    }

    function setAddress(string $address): void
    {
        $this->address = $address;
    }

    function setDate(string $date): void
    {
        $this->date = $date;
    }
}
