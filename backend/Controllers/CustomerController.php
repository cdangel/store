<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Repositories\CustomerRepository;
use App\Models\CustomerModel as Customer;

class CustomerController extends CustomerRepository
{
    public function index(): array
    {
        return parent::all();
    }

    public function show(array $params): array
    {
        $customer = new Customer();
        $customer->setId((int) $params['id']);

        return parent::find($customer);
    }

    public function store(array $params): array
    {
        $customer = new Customer();
        $customer->setName($params['name']);
        $customer->setLastname($params['lastname']);
        $customer->setAge((int) $params['age']);

        return parent::insert($customer);
    }

    public function update(array $params): array
    {
        $customer = new Customer();
        $customer->setId((int) $params['id']);
        $customer->setName($params['name']);
        $customer->setLastname($params['lastname']);
        $customer->setAge((int) $params['age']);

        return parent::change($customer);
    }

    public function delete(array $params): array
    {
        $customer = new Customer();
        $customer->setId((int) $params['id']);

        return parent::remove($customer);
    }
}
