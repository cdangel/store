<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Repositories\ProductRepository;
use App\Models\ProductModel as Product;

class ProductController extends ProductRepository
{
    public function index(): array
    {
        return parent::all();
    }

    public function show(array $params): array
    {
        $product = new Product();
        $product->setId((int) $params['id']);

        return parent::find($product);
    }

    public function store(array $params): array
    {
        $product = new Product();
        $product->setRef($params['ref']);
        $product->setName($params['name']);
        $product->setValue((int) $params['value']);

        return parent::insert($product);
    }

    public function update(array $params): array
    {
        $product = new Product();
        $product->setId((int) $params['id']);
        $product->setRef($params['ref']);
        $product->setName($params['name']);
        $product->setValue((int) $params['value']);

        return parent::change($product);
    }

    public function delete(array $params): array
    {
        $product = new Product();
        $product->setId((int) $params['id']);

        return parent::remove($product);
    }
}
