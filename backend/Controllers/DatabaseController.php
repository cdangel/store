<?php

namespace App\Controllers;

use PDO;

class DatabaseController
{
    private $conn;
    private $controller_db = 'pgsql';
    private $host_db       = 'localhost';
    private $port_db       = 5432;
    private $name_db       = 'store';
    private $user_db       = 'postgres';
    private $pass_db       = 'postgres';

    public function __construct()
    {
        $this->conn = new PDO("$this->controller_db:host=$this->host_db; dbname=$this->name_db; port=$this->port_db", "$this->user_db", "$this->pass_db");
        $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    }

    public function getConnection()
    {
        return $this->conn;
    }
}
