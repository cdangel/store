<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Repositories\SaleRepository;
use App\Models\SaleModel as Sale;

class SaleController extends SaleRepository
{
    public function index(): array
    {
        return parent::all();
    }

    public function show(array $params): array
    {
        $sale = new Sale();
        $sale->setId((int) $params['id']);

        return parent::find($sale);
    }

    public function store(array $params): array
    {
        $sale = new Sale();
        $sale->setAddress($params['address']);

        return parent::insert($sale)
        ;
    }

    public function update(array $params): array
    {
        $sale = new Sale();
        $sale->setId((int) $params['id']);
        $sale->setAddress($params['address']);

        return parent::change($sale);
    }

    public function delete(array $params): array
    {
        $sale = new Sale();
        $sale->setId((int) $params['id']);

        return parent::remove($sale);
    }

    public function reportSale(): array
    {
        return parent::report();
    }
}
