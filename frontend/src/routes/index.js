"use strict";

const { createRouter, createMemoryHistory } = VueRouter;

const router = new createRouter({
	history: createMemoryHistory(`store/frontend/public/`),
	routes: [
		{
			path: "/",
			name: "Home",
			component: () => import("../views/Home.js"),
		},
		{
			path: "/customers",
			name: "Customers",
			component: () => import("../views/Customers.js"),
		},
		{
			path: "/reports",
			name: "Reports",
			component: () => import("../views/Reports.js"),
		},
		{
			path: "/products",
			name: "Products",
			component: () => import("../views/Products.js"),
		},
		{
			path: "/sales",
			name: "Sales",
			component: () => import("../views/Sales.js"),
		}
	],
});

export default router;
