"use strict";

const App = {
	name: "App",
	/*html*/
	template: `
	<div>
		<div id="nav">
		<router-link to="/">Home</router-link>
		<router-link to="/customers">Customers</router-link>
		<router-link to="/products">Products</router-link>
		<router-link to="/sales">Sales</router-link>
		<router-link to="/reports">Reports</router-link>
		</div>
		<router-view />
	</div>
	`,
};

export default App;
