'use strict'

const reports = {
  namespaced: true,
  state: () => ({
    list: []
  }),
  mutations: {
    setList(state, data) {
      state.list = data
    }
  },
  actions: {
    async search({ commit }) {
      let req = await fetch('http://localhost:8080/store/backend/Routes/sale.php?report=1',{
        method: 'GET',
        mode: 'cors',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      })
      req = await req.json();
      commit('setList', req);
    }
  },
}

export default reports;