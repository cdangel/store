'use strict'

const products = {
  namespaced: true,
  state: () => ({
    product: {
      id: '',
      ref: '',
      name: '',
      value: '',
    },
    list: []
  }),
  mutations: {
    setList(state, data) {
      state.list = data
    }
  },
  actions: {
    async search({ commit }) {
      let req = await fetch('http://localhost:8080/store/backend/Routes/product.php',{
        method: 'GET',
        mode: 'cors',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      })
      req = await req.json();
      commit('setList', req)
    },

    async create({ dispatch, state }) {
      let req = await fetch('http://localhost:8080/store/backend/Routes/product.php',{
        method: 'POST',
        mode: 'cors',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          ref: state.product.ref,
          name: state.product.name,
          value: state.product.value
        })
      })
      req = await req.json();
      dispatch('search', req)
    },

    async update({ dispatch, state }) {
      let req = await fetch('http://localhost:8080/store/backend/Routes/product.php',{
        method: 'PUT',
        mode: 'cors',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          id: state.product.id,
          ref: state.product.ref,
          name: state.product.name,
          value: state.product.value
        })
      })
      req = await req.json();
      dispatch('search', req)
    },

    async del({ dispatch, state }) {
      let req = await fetch('http://localhost:8080/store/backend/Routes/product.php',{
        method: 'DELETE',
        mode: 'cors',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          id: state.product.id
        })
      })
      req = await req.json();
      dispatch('search', req)
    }
  },
}

export default products;