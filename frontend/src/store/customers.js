'use strict'

const customers = {
  namespaced: true,
  state: () => ({
    customer: {
      id: '',
      name: '',
      lastname: '',
      age: '',
    },
    list: []
  }),
  mutations: {
    setList(state, data) {
      state.list = data
    }
  },
  actions: {
    async search({ commit }) {
      let req = await fetch('http://localhost:8080/store/backend/Routes/customer.php',{
        method: 'GET',
        mode: 'cors',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        }
      })
      req = await req.json();
      commit('setList', req)
    },

    async create({ dispatch, state }) {
      let req = await fetch('http://localhost:8080/store/backend/Routes/customer.php',{
        method: 'POST',
        mode: 'cors',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          name: state.customer.name,
          lastname: state.customer.lastname,
          age: state.customer.age
        })
      })
      req = await req.json();
      dispatch('search', req)
    },

    async update({ dispatch, state }) {
      let req = await fetch('http://localhost:8080/store/backend/Routes/customer.php',{
        method: 'PUT',
        mode: 'cors',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          id: state.customer.id,
          name: state.customer.name,
          lastname: state.customer.lastname,
          age: state.customer.age
        })
      })
      req = await req.json();
      dispatch('search', req)
    },

    async del({ dispatch, state }) {
      let req = await fetch('http://localhost:8080/store/backend/Routes/customer.php',{
        method: 'DELETE',
        mode: 'cors',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          id: state.customer.id
        })
      })
      req = await req.json();
      dispatch('search', req)
    }
  },
}

export default customers;