"use strict";

const { Store } = Vuex;

import customers from './customers.js';
import products from './products.js';
import reports from './reports.js';

const store = new Store({
	state: () => ({}),
	mutations: {},
	actions: {},
	modules: { customers, reports, products },
});

export default store;
