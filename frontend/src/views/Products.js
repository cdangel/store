"use strict";

import ProductForm from "../components/products/ProductForm.js";
import ProductList from "../components/products/ProductList.js";

const Home = {
	name: "Home",
	components: { ProductList, ProductForm },
	setup() {},
	/*html*/
	template: `
		<product-form />
		<product-list />
	`,
};

export default Home;
