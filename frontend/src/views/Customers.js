"use strict";

import CustomerForm from "../components/customers/CustomerForm.js";
import CustomerList from "../components/customers/CustomerList.js";

const Home = {
	name: "Home",
	components: { CustomerForm, CustomerList },
	setup() {},
	/*html*/
	template: `
		<customer-form />
		<customer-list />
	`,
};

export default Home;
