"use strict";

import SaleForm from "../components/sales/SaleForm.js";
import SaleList from "../components/sales/SaleList.js";

const Sales = {
	name: "Sales",
	components: { SaleForm, SaleList },
	setup() {},
	/*html*/
	template: `
		<sale-form />
		<sale-list />
	`,
};

export default Sales;
