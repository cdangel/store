"use strict";

const { onMounted } = Vue;
const { useStore } = Vuex;

import ReportList from "../components/reports/ReportList.js";

const Home = {
	name: "Home",
	components: { ReportList },
	setup() {
		const store = useStore();
        const search = async () => await store.dispatch("reports/search");

		onMounted(async () => {
			await search();
		});
	},
	/*html*/
	template: `
		<report-list />
	`,
};

export default Home;
