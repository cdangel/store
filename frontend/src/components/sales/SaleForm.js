"use strict";

const { onMounted, toRef } = Vue
const { useStore } = Vuex

const SaleForm = {
	name: "SaleForm",
	setup() {
        const store = useStore();
        const search = async () => await store.dispatch("products/search");
        const searchCustomers = async () => await store.dispatch("customers/search");
        const customers = toRef(store.state.customers, "list")
        onMounted(async () => {
            await searchCustomers();
            await search();
        })
        return { customers }
    },
	/*html*/
	template: `
        <div>
            <select>
                <option v-for="(item, index) in customers" :key="index" :value="item.id">{{ item.name }} {{ item.lastname }}</option>
            </select>
        </div>
	`,
};

export default SaleForm;
