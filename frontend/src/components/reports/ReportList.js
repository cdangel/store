"use strict";

const { toRef } = Vue
const { useStore } = Vuex

const ReportList = {
	name: "ReportList",
    setup() {
        const store = useStore();
        const list = toRef(store.state.reports, "list")
        return { list }
    },
	/*html*/
	template: `
    <table>
        <tr>
            <td> Customer Id </td>
            <td> Customer Name </td>
            <td> Customer Lastname </td>
            <td> Customer Age </td>
            <td> Sale Id </td>
            <td> Sale Address </td>
            <td> Sale Date </td>
            <td> Product Id </td>
            <td> Product Ref </td>
            <td> Product Name </td>
            <td> Product Value </td>
            <td> Product Quantity </td>
            <td> Product Discount </td>
        </tr>
        <tr v-for="(item, index) in list" :key="index">
            <td> {{ item.customer_id }} </td>
            <td> {{ item.customer_name }} </td>
            <td> {{ item.customer_lastname }} </td>
            <td> {{ item.customer_age }} </td>
            <td> {{ item.sale_id }} </td>
            <td> {{ item.sale_address }} </td>
            <td> {{ item.sale_date }} </td>
            <td> {{ item.product_id }} </td>
            <td> {{ item.product_ref }} </td>
            <td> {{ item.product_name }} </td>
            <td> {{ item.product_value }} </td>
            <td> {{ item.product_quantity }} </td>
            <td> {{ item.product_discount }} </td>
        </tr>
    </table>
	`,
};

export default ReportList;
