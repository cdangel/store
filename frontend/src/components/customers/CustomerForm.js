"use strict";

const { toRef, computed } = Vue
const { useStore } = Vuex

const CustomerForm = {
	name: "CustomerForm",
	setup() {
        const store = useStore();
        const customer = toRef(store.state.customers, "customer")

        const search = async () => await store.dispatch("customers/search");
        const create = async () => await store.dispatch("customers/create");
        const update = async () => await store.dispatch("customers/update");
        const del = async () => await store.dispatch("customers/del");

        const valCreate = computed(() => {
        return (customer.value.name != '' && customer.value.lastname != '' && customer.value.age > 0)
        })

        const valUpdate = computed(() => {
        return (customer.value.id != '' && customer.value.name != '' && customer.value.lastname != '' && customer.value.age > 0)
        })

        const valDelete = computed(() => {
        return (customer.value.id != '')
        })

        return { customer, valCreate, valUpdate, valDelete, search, create, update, del }
    },
	/*html*/
	template: `
        <div>
            <input v-model="customer.id" type="text" placeholder="Id" />
            <input v-model="customer.name" type="text" placeholder="Name" />
            <input v-model="customer.lastname" type="text" placeholder="Lastname" />
            <input v-model="customer.age" type="number" placeholder="Age" />

            <button @click="search">Search</button>
            <button @click="create" :disabled="!valCreate">Create</button>
            <button @click="update" :disabled="!valUpdate">Update</button>
            <button @click="del" :disabled="!valDelete">Delete</button>
        </div>
	`,
};

export default CustomerForm;
