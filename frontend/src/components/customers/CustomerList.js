"use strict";

const { toRef } = Vue
const { useStore } = Vuex

const CustomerForm = {
	name: "CustomerForm",
    setup() {
        const store = useStore();
        const list = toRef(store.state.customers, "list")
        return { list }
    },
	/*html*/
	template: `
    <table>
        <tr>
            <td> Id </td>
            <td> Name </td>
            <td> Lastname </td>
            <td> Age </td>
        </tr>
        <tr v-for="(item, index) in list" :key="index">
            <td> {{ item.id }} </td>
            <td> {{ item.name }} </td>
            <td> {{ item.lastname }} </td>
            <td> {{ item.age }} </td>
        </tr>
    </table>
	`,
};

export default CustomerForm;
