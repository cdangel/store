"use strict";

const { toRef, computed } = Vue
const { useStore } = Vuex

const ProductForm = {
	name: "ProductForm",
	setup() {
        const store = useStore();
        const product = toRef(store.state.products, "product")

        const search = async () => await store.dispatch("products/search");
        const create = async () => await store.dispatch("products/create");
        const update = async () => await store.dispatch("products/update");
        const del = async () => await store.dispatch("products/del");

        const valCreate = computed(() => {
        return (product.value.ref != '' && product.value.name != '' && product.value.value > 0)
        })

        const valUpdate = computed(() => {
        return (product.value.id != '' && product.value.ref != '' && product.value.name != '' && product.value.value > 0)
        })

        const valDelete = computed(() => {
        return (product.value.id != '')
        })

        return { product, valCreate, valUpdate, valDelete, search, create, update, del }
    },
	/*html*/
	template: `
        <div>
            <input v-model="product.id" type="text" placeholder="Id" />
            <input v-model="product.ref" type="text" placeholder="Ref" />
            <input v-model="product.name" type="text" placeholder="Name" />
            <input v-model="product.value" type="number" placeholder="Value" />

            <button @click="search">Search</button>
            <button @click="create" :disabled="!valCreate">Create</button>
            <button @click="update" :disabled="!valUpdate">Update</button>
            <button @click="del" :disabled="!valDelete">Delete</button>
        </div>
	`,
};

export default ProductForm;
