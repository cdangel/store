"use strict";

const { toRef } = Vue
const { useStore } = Vuex

const ProductList = {
	name: "ProductList",
    setup() {
        const store = useStore();
        const list = toRef(store.state.products, "list")
        return { list }
    },
	/*html*/
	template: `
    <table>
        <tr>
            <td> Id </td>
            <td> Ref </td>
            <td> Name </td>
            <td> Value </td>
        </tr>
        <tr v-for="(item, index) in list" :key="index">
            <td> {{ item.id }} </td>
            <td> {{ item.ref }} </td>
            <td> {{ item.name }} </td>
            <td> {{ item.value }} </td>
        </tr>
    </table>
	`,
};

export default ProductList;
